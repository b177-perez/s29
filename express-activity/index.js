// s29 Academic Break 0603 Activity Express.js

const express = require("express");

const app = express();

const port = 4001;


app.use(express.json());

app.use(express.urlencoded({extended:true}));



// Create a POST route

app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})



let users = [];


// Create a POST route to register a user

app.post("/register", (req, res) => {
	console.log(req.body);

	if(req.body.firstName !== '' && req.body.lastName !== '' && req.body.username !== '' && req.body.password !== ''){

		users.push(req.body);

		res.send(`User ${req.body.username} successfully registered!`)
	}
	else {
		res.send(`Please input values to ALL fields: firstName, lastName, username and password.`)
	}

});


// POSTMAN
// s29 AB 0603 Activity - express-activity
// POST Route express-activity /register



app.post("/login", (req, res) => {


    let message;

    // Creates a condition if there are users found in the array
    if (users.length != 0){

        // Creates a for loop that will loop through the elements of the "users" array
        for(let i = 0; i < users.length; i++){

            // If the username provided in the client/Postman and the username of the current object in the loop is the same
            if (req.body.username == users[i].username && req.body.password == users[i].password) {


                // Changes the message to be sent back by the response
                message = `User ${req.body.username} has successfully login.`;

                break;

            } 


            // If username exist but incorrect password.
            else if (req.body.username == users[i].username && req.body.password != users[i].password) {


                // Changes the message to be sent back by the response
                message = `Incorrect username or password.`;

                break;

            }

    		// If incorrect username.
            else {


                // Changes the message to be sent back by the response
                message = `User does not exist.`;

                break;

            }

        }

    // If no user was found
    } else {

        // Changes the message to be sent back by the response
        message = "User does not exist.";

    }
    
    // Sends a response back to the client/Postman once the user has been deleted or if a user is not found
    res.send(message);

})


// POSTMAN
// s29 AB 0603 Activity - express-activity
// POST Route express-activity /login



// Tells our server to listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));

