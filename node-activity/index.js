// s29 Activity - Acadrmic Break 0603

// console.log("Hello World");


// Create a fetch request using GET method that will retrieve a single to do list item.

// Using the data retrieved, print a message in the console that will provide the title and body of the post item.

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(`The title of the post is "${json.title}" and the body of the post is "${json.body}".`));



// Create a fetch request using PATCH method that will update a post item using the same JSON placeholder.

// Update a post item by changing the title and the body.

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated Title of Post Item',
		body: 'This is the updated body of the post item'
	}),

})

.then((response) => response.json())
.then((json) => console.log(json));


// Create the requests via Postman to retrieve and update a post.

// Export the postman collection and save it in the node-activity folder.

// POSTMAN
// s29 AB 0603 Activity - node-activity
// GET Route node-activity
// PATCH Route node-activity









